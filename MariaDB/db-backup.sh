#!/bin/bash

# Todo

# Lage en rutine som gjør at hver måned så blir det tatt en såkalt månedsbackup.
# Den kan f.eks hete databasenavn-oktober-2024.sql.gz
# En trenger bare å bestemme en dato, f.eks den 12. og så i rutinen sjekke om det er den 12 og dermed kopiere dagens backup til månedsbackupen.

# vurdering rundt --lock-tables: om natten lik true, om dagen false. Det betyr at en vil kunne kjøre backup flere ganger i døgnet
# droppe dump filer inn på billy området for langtidslagring!

# En må restarte rett før backup. Om antall workers har nådd taket, så blir det ingen backup
#service mariadb restart

# Skal en reparere først?
# Ser ut til at mysql dør... hvorfor????
# mysqlcheck en og en database høres ut som en plan
#mysqlcheck -u root --auto-repair --check --all-databases &> /backup/helge/autorepairloggen.$(date -I)

#DATOTID=$(date '+%Y-%m-%d_%H:%M:%S')
DATOTID=$(date +%Y-%m-%d_%T)
MAANED=$(date +%B)
DAG=$(date +%d)
AAR=$(date +'%Y')

LISTA=`find /var/lib/mysql/ -maxdepth 1 -type d | awk -F '/' '{print $5}' | sort | uniq | sed '/^$/d'`

for i in $LISTA; do echo "cd /backup/helge; mysqldump --lock-tables=false $i  > /backup/helge/$i.$DATOTID.sql"; done > /backup/helge/runme.sh
echo "cd /backup/helge/; gzip *.sql" >> /backup/helge/runme.sh

# Månedsbackup
# Dersom gitt dagsnummer, kopier dump fil til månedens fil
if [ $DAG = "09" ]; then
  for i in $LISTA; do echo "mv /backup/helge/$i.$DATOTID.sql.gz /backup/helge/$i.$AAR.$MAANED.sql.gz"; done >> /backup/helge/runme.sh
fi

#if [ $DAG = "09" ]; then
#  echo "mv /backup/helge/$i.$DATOTID.sql.gz /backup/$i.$MAANED.sql.gz" >> /backup/helge/runme.sh
#fi

# kjoerbart script
chmod 755 /backup/helge/runme.sh

# Kjør backup
/backup/helge/runme.sh


# til slutt ta vare på kommandoene i en git log
cd /backup/helge/
git commit -m "runme.sh: db-backup kommandoene" runme.sh

exit;
