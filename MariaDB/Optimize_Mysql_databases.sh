#!/bin/bash
mysqlcheck --optimize --auto-repair --databases mysql
mysqlcheck --optimize --auto-repair --databases performance_schema
mysqlcheck --optimize --auto-repair --databases helge
mysqlcheck --optimize --auto-repair --databases boolean
mysqlcheck --optimize --auto-repair --databases corviz
mysqlcheck --optimize --auto-repair --databases birkeland
#mysqlcheck --optimize --databases booking2
mysqlcheck --optimize --auto-repair --databases bergmus
mysqlcheck --optimize --auto-repair --databases emid
mysqlcheck --optimize --auto-repair --databases discretion
mysqlcheck --optimize --auto-repair --databases turspor
mysqlcheck --optimize --auto-repair --databases vis
#mysqlcheck --optimize --databases btrc_chem
#mysqlcheck --optimize --databases btrc_impl
mysqlcheck --optimize --auto-repair --databases dna
mysqlcheck --optimize --auto-repair --databases skeivtarkiv20220301
mysqlcheck --optimize --auto-repair --databases expearth
mysqlcheck --optimize --auto-repair --databases mso
#mysqlcheck --optimize --databases nmr
#mysqlcheck --optimize --databases wordpresstest3
mysqlcheck --optimize --auto-repair --databases skeivtarkiv

