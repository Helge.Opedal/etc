#!/bin/bash

HOSTNAME=`hostname`

# Fjerner gamle og unødvendige logg filer. Bør kjøres fra cron jevnlig, f.eks hver søndag.

# Solr filer
find /dspace/bora/build/log -type f -name 'solr.log.*' -mtime +7 -exec rm -f {} \;

# Tomcat logg filer (catalina.out)
find /var/log/tomcat6 -type f -name 'catalina.201*' -mtime +7 -exec rm -f {} \;

if [ $HOSTNAME == "dspace9.uib.no" ]; then
  TEST="ja";
fi

if [ TEST == "ja" ]; then
  # Fjern litt flere unødvendige logg-filer
  find /var/log/tomcat6/ -type f -mtime +3 -exec rm -f {} \;
fi
