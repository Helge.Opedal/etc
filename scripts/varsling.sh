#!/bin/bash
# er nettstedet tilgjengelig?
# Sjekker hvert 5 minutt. Sender epost. Dersom epost er sendt, så sendes ikke ny epost før om 1 time.
# Tanken er at vi kan få lagt inn at en skal starte Postgresql på nytt igjen også...
# men først må vi være sikre på at varslingen fungerer.

KATALOGEN="/dspace/scripts/tester"
URLEN="http://bora.uib.no"
FILA="htmlfila"
LOGGFILA="loggfila"
DATO=`date`
#EPOSTLISTE="mihho@uib.no,Irene.Eikefjord@uib.no,Tarje.Lavik@uib.no,Oyvind.Gjesdal@uib.no,Hemed.Ruwehy@uib.no"
#EPOSTLISTE="mihho@uib.no,Irene.Eikefjord@uib.no,Tarje.Lavik@uib.no,Trond.Davidsen@uib.no"
# EPOSTLISTE="mihho@uib.no,mgpjs@uib.no"
EPOSTLISTE="mihho@uib.no"

INFOTEKST=`cat $KATALOGEN/infotekst.txt`;
INFOFRALOGGFILA=`cat $KATALOGEN/$LOGGFILA`

# hent ned en side
wget -q --tries=5 --timeout=180 -O $KATALOGEN/$FILA $URLEN

# Hvor mange linjer med GET solr requests fra localhost? Solr indexering?
#ANTLINJER=`cat /var/log/httpd/access_log | grep GET | grep '127.0.0.1' | grep solr | wc -l`

# Finn første dato i access logg fila og siste, deretter 10 på topp listen
TIPAATOPP=`cat /var/log/httpd/access_log | awk {'print $1'} | sort | uniq -c | sort -n -r | head -10`
FRA=`head -n 1 /var/log/httpd/access_log | awk {'print $4'} | sed 's/^\[//g'`
TIL=`tail -2 /var/log/httpd/access_log | head -1 | awk {'print $4'} | sed 's/^\[//g'`

# SSL REQUEST FILA
SSLREQTIPAATOPP=`cat /var/log/httpd/ssl_request_log | awk {'print $3'} | sort | uniq -c | sort -n -r | head -10`
SSLREQFRA=`head -n 1 /var/log/httpd/ssl_request_log | awk {'print $1'} | sed 's/^\[//g'`
SSLREQTIL=`tail -2 /var/log/httpd/ssl_request_log | head -1 | awk {'print $1'} | sed 's/^\[//g'`


#TEKST="Bora ser ut til å være nede.\n\nSjekk http://bora.uib.no \n\nAntall linjer i accessloggen er: $ANTLINJER.\n\n$INFOTEKST.\n\nTi på topp listen fra $FRA til $TIL er: $TIPAATOPP."
echo -e "Bora ser ut til å være nede.\n\nRestarter Apache og Tomcat webserver.--\nSjekk http://bora.uib.no \n\nSjekk Metiri også: https://metiri.uib.no/Linux/bibliotheca.uib.no/index.html\n\n$INFOTEKST.\n\nTi på topp listen fra $FRA til $TIL er:\n$TIPAATOPP." > $KATALOGEN/informasjon.txt
echo -e "I tillegg så er det en god del trafikk som vises i ssl_request fila:\n\n Ti på topp i ssl_request er fra $SSLREQFRA til $SSLREQTIL:\n\n $SSLREQTIPAATOPP\n\n." >> $KATALOGEN/informasjon.txt


if [ `cat $KATALOGEN/$FILA | grep 'Faculty of Medicine' | wc -l` -eq 0 ];then
  # Noe er galt, ingen linjer, vi må varsle. Har vi en fil med navn "varslet" som er eldre enn 1 eller 2 timer? I så fall må vi varsle en gang til.
  if [ `find $KATALOGEN -type f -name 'varslet' -cmin +120 | wc -l` -eq 1 ];then
    #echo -e $TEKST | mutt -s "Bora er nede." $EPOSTLISTE
    cat $KATALOGEN/informasjon.txt | mutt -s "Bora er nede." $EPOSTLISTE
    service httpd stop
    service tomcat6 stop
    service tomcat6 start
    service httpd start
    echo -e "Bora var nede $DATO" >> $KATALOGEN/$LOGGFILA
    rm -f $KATALOGEN/varslet
    touch $KATALOGEN/varslet
  fi

  # Dersom det ikke er noen  fil med navn "varsel", så må vi også varsle
  if [ ! -f $KATALOGEN/varslet ];then
    #echo -e $TEKST | mutt -s "Bora er nede." $EPOSTLISTE
    cat $KATALOGEN/informasjon.txt | mutt -s "Bora er nede." $EPOSTLISTE
    echo "Bora var nede $DATO" >> $KATALOGEN/$LOGGFILA
    touch $KATALOGEN/varslet
  fi
fi
