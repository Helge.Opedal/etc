#!/bin/bash

# Av en mystisk grunn så floodes log fila. Har prøvd alt, men ikke søren om den vil slutte.
# Nå kjører jeg denne hvert minutt, slik at ikke disken går full

# Path to the log file
LOG_FILE="/var/log/php-fpm/www-error2.log"

# Remove lines containing 'preg_match()' m.m. directly from the log file
sed -i '/All-in-One Event Calendar/d' "$LOG_FILE"
sed -i '/Custom error/d' "$LOG_FILE"
sed -i '/wassup/d' "$LOG_FILE"
sed -i '/deprecated/d' "$LOG_FILE"
sed -i '/preg_match/d' "$LOG_FILE"
sed -i '/OLD/d' "$LOG_FILE"
sed -i '/BACKUP/d' "$LOG_FILE"
sed -i '/qtranxf_postsFilter/d' "$LOG_FILE"
