#!/bin/bash

# debug
#set -x

# Under konstruksjon...
# Lennart&Helge

## This scripts dumps all the databases in a Postgres 9.2 server on RHEL7, localhost
## 2 dump files are made for each database. One with Inserts another without.

## TODO
# - scriptet krever både mail og bzip. Yum install? som må kjøres som root, mens scriptet kjører som postgres. Hmmm...
# - versjonssjekk, hvilken postgresql kjøres?
# - implement a 'if system is Test' option to minimize number of dump files UNDER PROGRESS
# - use functions
# - some kind of integration with Jenkins?
# - fix the 2 strange '|' that appears in the DATABASE list FIXED?
# - Add timer so we can optimize speed of the script execution time
# - enable use of the logfile LOGFILE. Could be nice to log what this script is/has been doing and when.
# - number of days to keep a dump file could be a parameter to this script
# - enable print of name of the script, where the script is run (hostname and directory). Makes it easy to find the script on a server
# - would be nice to add a incremental feature for this script. Then one can dump files several times a day, without worrying about space problems on the harddisk
## TODO END
logTag='UIB_APP_PG-Backup'
logger -t $logTag "[START] PostgreSQL backup."
# Timer
start_time=$(date +%s)

#rpm -qa | grep -qw mailx || yum install -y mailx
#rpm -qa | grep -qw bzip2 || yum install -y bzip2

# Variables
LOGFILE="/backup/postgresql_dumps/pgsql_dump.log"
BACKUP_DIR="/backup/postgresql_dumps"
BACKUP_DIR2="backup/postgresql_dumps"
HOSTNAME=`hostname`
MAILLIST="mihho@uib.no" # should be edited
# Is this a test system? Set TESTSYSTEM to 'yes' in order to remove date and time information from dumpfile names (in order to minimize number of dumpfiles).
TESTSYSTEM="no"
TODAY=$(date|awk '{ print $1 }')
MONTH=$(date|awk '{ print $1 }')
MONTHNAME=`date +%b --date '0 month'`
DAYINMONTH=$(date|awk '{ print $3 }')
YEAR=$(date | awk '{ print $6 }')

# Only postgres can run this script
if [ `whoami` != "postgres" ]; then
  echo "pgsql_dump tried to run, but user is not postgres!" >> $LOGFILE
  echo "You are not postgres, can not run."
  echo "Try: su -c ./pgsql_dump.sh postgres"
  exit;
fi

# Check if there any backup files. If not, something is wrong!
if [ `find $BACKUP_DIR/ -type f -name '*.sql.bz2' -mtime -2 | wc -l` -eq 0 ]; then
  echo "There are no pgsql dumps for the last 2 days at $HOSTNAME. Something is wrong!" | mail -s "[PGSQLDUMP ERROR]  $HOSTNAME" $MAILLIST
  echo "[ERROR] There are no pgsql dumps for the last 2 days at $HOSTNAME. Something is wrong!" | logger -t $logTag 
fi

# logfile might be nice to have (or maybe Jenkins is the way to go?)
if [ ! -e $LOGFILE ]; then
  touch $LOGFILE
fi

if [ $TESTSYSTEM == "yes" ];then
  #DATABASES=`psql -q -c "\l" | sed -n 4,/\eof/p | grep -v rows | grep -v template0 | awk {'print $1}' | sed 's/^://g' | sed -e '/^$/d' | grep -v '|'`
  # For testing purposes
  DATABASES="itwiki
  helgewiki"
else
  DATABASES=`psql -q -c "\l" | sed -n 4,/\eof/p | grep -v rows | grep -v template0 | awk {'print $1}' | sed 's/^://g' | sed -e '/^$/d' | grep -v '|'`
fi

for i in $DATABASES; do

  ## Create folders per database if they don't exist
  if [ ! -d "$BACKUP_DIR/$i/" ];then
    mkdir -p $BACKUP_DIR/$i
  fi
  if [ ! -d "$BACKUP_DIR/$i/daily" ];then
    mkdir -p $BACKUP_DIR/$i/daily
  fi
  if [ ! -d "$BACKUP_DIR/$i/monthly" ];then
    mkdir -p $BACKUP_DIR/$i/monthly
  fi

  # On Test servers we don't want dump files with date and time information
  if [ $TESTSYSTEM == "yes"  ];then
    DAILYFILENAME="daily_$i"
    MONTHLYFILENAME="monthly_$i"
    ALLDATABASESFILENAME="all-databases"
  else
    DAILYFILENAME="daily_$i_$TODAY"
    MONTHLYFILENAME="monthly_$i_$MONTHNAME"
    ALLDATABASESFILENAME="all-databases_$TODAY"
  fi

  # backup for each weekday (Mon, Tue, ...) plain and dump
  nice -n 10 /usr/bin/pg_dump --column-inserts $i > $BACKUP_DIR/$i/daily/"$DAILYFILENAME".sql && logger -t ${logTag} "Plain backup success: $i"
  nice -n 10 /usr/bin/pg_dump -C -Fc $i > $BACKUP_DIR/$i/daily/"$DAILYFILENAME".dump && logger -t ${logTag} "Binary dump backup success: $i"
  #nice -n 10 tar cjf $BACKUP_DIR/$i/daily/"$DAILYFILENAME".sql.bz2 -C / $BACKUP_DIR2/$i/daily/"$DAILYFILENAME".sql
  nice -n 10 bzip2 -f $BACKUP_DIR/$i/daily/"$DAILYFILENAME".sql
  #rm -f $BACKUP_DIR/$i/daily/"$DAILYFILENAME".sql

  # dump with copy statements
  #nice -n 10 /usr/bin/pg_dump $i > $BACKUP_DIR/$i/daily/"$DAILYFILENAME"_copy.sql
  #nice -n 10 tar cjf $BACKUP_DIR/$i/daily/"$DAILYFILENAME"_copy.sql.bz2 -C / $BACKUP_DIR2/$i/daily/"$DAILYFILENAME"_copy.sql
  #nice -n 10 bzip2 -f $BACKUP_DIR/$i/daily/"$DAILYFILENAME"_copy.sql
  #rm -f $BACKUP_DIR/$i/daily/"$DAILYFILENAME"_copy.sql

  # monthly backup (Jan, Feb...)
  if [ $DAYINMONTH==10 ]; then
    cp -f $BACKUP_DIR/$i/daily/"$DAILYFILENAME".sql.bz2 $BACKUP_DIR/$i/monthly/"$MONTHLYFILENAME".sql.bz2
    cp -f $BACKUP_DIR/$i/daily/"$DAILYFILENAME"_copy.sql.bz2 $BACKUP_DIR/$i/monthly/"$MONTHLYFILENAME"_copy.sql.bz2
  fi

  # Year backup
  # coming after a while

done

## Full backup
nice -n 10 /usr/bin/pg_dumpall --column-inserts  > $BACKUP_DIR/"$ALLDATABASESFILENAME".sql
nice -n 10 /usr/bin/pg_dumpall  > $BACKUP_DIR/"$ALLDATABASESFILENAME"_copy.sql

#nice -n 10 tar cjf $BACKUP_DIR/"$ALLDATABASESFILENAME".sql.bz2 -C / backup/postgresql_dumps/"$ALLDATABASESFILENAME".sql
#nice -n 10 tar cjf $BACKUP_DIR/"$ALLDATABASESFILENAME"_copy.sql.bz2 -C / backup/postgresql_dumps/"$ALLDATABASESFILENAME"_copy.sql

nice -n 10 bzip2 -f $BACKUP_DIR/"$ALLDATABASESFILENAME".sql
nice -n 10 bzip2 -f $BACKUP_DIR/"$ALLDATABASESFILENAME"_copy.sql

# Copy to all-databases-latest.sql.bz2
rsync -a $BACKUP_DIR/"$ALLDATABASESFILENAME".sql.bz2 $BACKUP_DIR/all-databases-latest.sql.bz2
bunzip2 $BACKUP_DIR/all-databases-latest.sql.bz2
zip $BACKUP_DIR/all-databases-latest.sql.zip $BACKUP_DIR/all-databases-latest.sql
#mv $BACKUP_DIR/all-databases-latest.sql.zip $BACKUP_DIR/transfer/

#rm -f $BACKUP_DIR/"$ALLDATABASESFILENAME".sql
#rm -f $BACKUP_DIR/"$ALLDATABASESFILENAME"_copy.sql

## Vacuuming
nice -n 10 vacuumdb -a -f -z -q

finish_time=$(date +%s)
echo "Time duration for pg_dump script at $HOSTNAME: $((finish_time - start_time)) secs." | mail $MAILLIST

logger -t $logTag "[FINISHED] PostgreSQL backup."
