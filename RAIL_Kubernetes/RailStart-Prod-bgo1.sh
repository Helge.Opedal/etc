#!/bin/bash
RAIL_PORT=$(ssh helgeo@login-01.bgo1.prod.rail.uhdc.no grep -- --listen-address=127.0.0.1: .kube/config | cut -d: -f2)
ssh -L$RAIL_PORT:localhost:$RAIL_PORT helgeo@login-01.bgo1.prod.rail.uhdc.no kubectl get pods
firefox http://localhost:22008
ssh -i ~/.ssh/id_rsa_rail helgeo@login-01.bgo1.prod.rail.uhdc.no
