#!/bin/bash

DATO=$(date "+%Y-%m-%d")

> /root/scripts/tmp/runme.sh

mkdir -p /root/scripts/tmp/

## VIRKER IKKE? HMM HVORFOR IKKE
#find /backup/ -maxdepth 0 -type f -name '*.tar' -mtime +1 -exec rm -f {} \;

## DENNE VIRKER KANSKJE... VI KAN SPARE PÅ SMÅFILER
## Opprydningsscript burde egentlig kjøre i egne bash script... JEPP!
#find /backup -maxdepth 1 -type f -name '*.tar' -mtime +2 -size +400M -exec rm -f {} \;
#find /backup -maxdepth 1 -type f -name '*.tar.gz' -mtime +2 -size +100M -exec rm -f {} \;
#find /backup/wordpress/ -maxdepth 1 -type f -name '*.tar' -mtime +30 -exec rm -f {} \;

for i in $(find /var/www/sites/ -maxdepth 3 -type f -name 'wp-config.php' | awk -F '/' '{print $5}'); do
  echo "cd /var/www/sites/; tar -cf /backup/$i$DATO.tar $i/;";
  echo "cd /var/www/sites/; gzip /backup/$i$DATO.tar;";
done > /root/scripts/tmp/runme.sh

for i in $(find /var/www/sites/ -maxdepth 3 -type f -name 'wp-config.php' | awk -F '/' '{print $5}'); do
  echo "cd /var/www/sites/$i/htdocs/; wp core update --allow-root";
done >> /root/scripts/tmp/runme.sh

for i in $(find /var/www/sites/ -maxdepth 3 -type f -name 'wp-config.php' | awk -F '/' '{print $5}'); do
  echo "cd /var/www/sites/$i/htdocs/; wp plugin update-all --allow-root";
done >> /root/scripts/tmp/runme.sh

for i in $(find /var/www/sites/ -maxdepth 3 -type f -name 'wp-config.php' | awk -F '/' '{print $5}'); do
  echo "cd /var/www/sites/$i/htdocs/; wp theme update-all --allow-root";
done >> /root/scripts/tmp/runme.sh

for i in $(find /var/www/sites/ -maxdepth 3 -type f -name 'wp-config.php' | awk -F '/' '{print $5}'); do
  echo "cd /var/www/sites/$i/htdocs/; wp core update-db --allow-root";
done >> /root/scripts/tmp/runme.sh

# expearth i særstilling
echo "cd /var/www/sites/; tar -cf /backup/expearth$DATO.tar expearth/;" >> /root/scripts/tmp/runme.sh
echo "cd /var/www/sites/; gzip /backup/expearth$DATO.tar;" >> /root/scripts/tmp/runme.sh
echo "cd /var/www/sites/expearth/htdocs/wordpress/; wp core update --allow-root;" >> /root/scripts/tmp/runme.sh
echo "cd /var/www/sites/expearth/htdocs/wordpress; wp plugin update-all --allow-root" >> /root/scripts/tmp/runme.sh
echo "cd /var/www/sites/expearth/htdocs/wordpress; wp theme update-all --allow-root" >> /root/scripts/tmp/runme.sh
echo "cd /var/www/sites/expearth/htdocs/wordpress; wp core update-db --allow-root" >> /root/scripts/tmp/runme.sh

chmod 750 /root/scripts/tmp/runme.sh

