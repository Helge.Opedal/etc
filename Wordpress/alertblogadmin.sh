#!/bin/bash

# inn parameter epost til wordpress admin

TIL=$1

echo "echo \"Sender nå epost til $TIL\""

# Sett riktig locale
export LANG="nn_NO.utf8"
echo -e "export LANG=\"nn_NO.utf8\""

# refresh ldap cache, må kjøres før scriptet kjøres
sss_cache -E

# dato
now=`date +"%-d. %B %Y"`

# avsender epost
export EMAIL="Wordpress drift<hotelsjef@it.uib.no>"
echo -e "export EMAIL=\"Wordpress drift<hotelsjef@linuxdrift.uib.no>\""

echo -e "echo -e \"English below.
Hei,\nDette er en melding fra IT avdelingen - seksjon for Applikasjoner, Wordpress/Webhotell drift.
Du mottar denne eposten da du er registrert som bruker av en wordpress nettside under w.uib.no.
Vi opplever hvert år flere bruteforce angrep fra datamaskiner fra hele verden, og ser nå at vi er nødt til å endre innloggingslenken til wordpress nettsidene.

Tidligere har en logget inn via wp-login.php.

Dette er nå endret til:
logginnuib

Dersom du har en blogg som heter eksempelvis:
minblogg.w.uib.no

så skal du nå skrive:
minblogg.w.uib.no/logginnuib

for å kunne logge inn.

Denne endringen blir gjennomført innen kort tid og etter endringen vil innlogging via wp-login.php slutte å virke.

Du kan ikke svare på denne eposten. Om du har spørsmål er det fint om du sender inn en sak via TopDesk (hjelp.uib.no).

Med vennlig hilsen
IT avdelingen - UiB
\n$now\nit.uib.no  


English:

Hi,
This is a message from UiB IT department.
You receive this email because your email address is registered as a user of our Wordpress w.uib.no blog system.

The login link wp-login.php will be changed to:

uiblogin

This means that you have to use: yoursitew.uib.no/logginnuib

to be able to log in.

The reason for this change is that we are under a brute-force attack from a large botnet and we need to hide the login link.

You can not reply to this email. If you have any questions, please open an issue at UiB TopDesk.

Best regards,

IT department - University of Bergen
$now
\" | mutt -e \"set send_charset='utf-8'\" -s \"Ny innloggingslenke til wordpress nettsider under w.uib.no - New login url for UiB Wordpress websites\" $TIL"

echo -e "sleep 1"
