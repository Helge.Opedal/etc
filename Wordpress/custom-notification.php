<?php
/*
Plugin Name: Custom Notification Banner
Description: Displays a notification banner in the admin dashboard for upcoming upgrades.
*/

// Add a notification banner to the admin dashboard
function add_custom_notification_banner() {
    // Check if the user has admin privileges
    if (current_user_can('manage_options')) {
        $upgrade_notice = '<div class="notice notice-info is-dismissible">
Informasjon om tjenesten: <a target=_blank href="https://it.uib.no/Wordpress">https://it.uib.no/Wordpress</a>
<br><i>(internt: rhel8-wp-blog04)</i>)</font></p>
        </div>';

        echo $upgrade_notice;
    }
}
add_action('admin_notices', 'add_custom_notification_banner');

