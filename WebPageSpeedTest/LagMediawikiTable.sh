#!/bin/bash

cd /home/helgeo/WebPageSpeedTest/
# Input file containing your data
INPUT_FILE="GjennomSnittResultat"

# Print the MediaWiki table header
echo "{| class=\"wikitable sortable\""
echo "|+ Performance Data"
echo "|-"
echo "! Nettside/domene !! Gjsn (sek) !! Min (sek) !! Maks (sek) !! StAvvik (sek)"

# Read the CSV file line by line
tail -n +2 "$INPUT_FILE" | while IFS=, read -r nettside gjsn min maks st_avvik; do
    echo "|-"
    echo "| $nettside || $gjsn || $min || $maks || $st_avvik"
done

# Print the table closing tag
echo "|}"

