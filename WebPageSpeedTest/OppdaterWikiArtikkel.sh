#!/bin/bash
# Oppdaterer artikkel i itwiki'en med gjennomsnittlig svar respons på diverse nettsider
# Denne siden: https://itwiki.uib.no/Nettside_svar_respons
# Se også /home/helgeo/WebPageSpeedTest
cd /var/www/app/wiki/sites/itwiki/code/
/bin/php83 maintenance/edit.php --summary="Speed test resultat" "Nettside svar respons" - < /home/helgeo/WebPageSpeedTest/GjennomSnittResultatMediawikiTabell
