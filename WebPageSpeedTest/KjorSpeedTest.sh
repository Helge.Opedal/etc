#!/bin/bash

# Loop gjennom domeneliste lag kommandoene
# Resultatet skrive til runme.sh
# ./WebPageSpeedTest.sh https://www.uib.no &> resultat; cat resultat | egrep 'clock time|unable'

cd /home/helgeo/WebPageSpeedTest/

for domene in $(cat DomeneLista); do
  #echo "echo $domene;";
  #echo "./WebPageSpeedTest.sh $domene &> resultat; cat resultat | egrep 'clock time|unable'";
  DOMENE=$domene;
  RESULTAT=$(./WebPageSpeedTest.sh $domene &> resultat; cat resultat | egrep 'clock time|unable' | sed 's/Total wall clock time: //g');
  echo "$DOMENE: $RESULTAT\n" >> SpeedTestResultat
done

# Regn ut gjennomsnitt!
/home/helgeo/WebPageSpeedTest/GjennomSnitt.sh
