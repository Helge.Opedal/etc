#!/bin/bash

# Check if URL parameter is provided
if [ -z "$1" ]; then
  echo "Usage: $0 <URL>"
  exit 1
fi

# URL of the webpage to download
URL="$1"

# Output directory for the downloaded files
OUTPUT_DIR="website_download"

# Timeout duration (in seconds)
TIMEOUT=30

# Number of retries
TRIES=1

# Function to download the webpage and its assets
download_page_assets() {
    wget -p -k -E -P "$OUTPUT_DIR" --timeout=$TIMEOUT --tries=$TRIES "$URL"
}

# Measure the time taken for the download
echo "Starting download..."
START_TIME=$(date +%s)
download_page_assets
END_TIME=$(date +%s)
ELAPSED_TIME=$((END_TIME - START_TIME))

# Convert elapsed time to human-readable format
ELAPSED_TIME_HUMAN=$(printf '%02d:%02d:%02d\n' $((ELAPSED_TIME/3600)) $((ELAPSED_TIME%3600/60)) $((ELAPSED_TIME%60)))

echo "Download completed."
echo "Total time taken: $ELAPSED_TIME_HUMAN"

# Output download location
echo "Downloaded files are located in the directory: $OUTPUT_DIR"

