#!/bin/bash

# Define the input file
input_file="SpeedTestResultat"
output_file="GjennomSnittResultat"
temp_file="tmp1"

# Use awk to process the file and calculate the required statistics for each domain
awk '
{
    # Replace colon with comma, replace comma with dot, and remove trailing "s"
    gsub(":", " ", $0);
    gsub(",", ".", $2);
    gsub("s", "", $2);

    # Initialize arrays and variables
    domain = $1;
    response_time = $2 + 0;  # Convert response time to numeric

    # Check if the response time is 0.0 and set it to 0.1 if true
    if (response_time == 0.0) {
        response_time = 0.1;
    }

    # Add the response time to the sum and increment the count for each domain
    sum[domain] += response_time;
    count[domain] += 1;
    times[domain][count[domain]] = response_time;

    # Calculate the minimum and maximum response times
    if (!(domain in min) || response_time < min[domain]) {
        min[domain] = response_time;
    }
    if (!(domain in max) || response_time > max[domain]) {
        max[domain] = response_time;
    }
}
END {
    # Calculate the average and standard deviation for each domain
    for (domain in sum) {
        avg = sum[domain] / count[domain];

        # Calculate the standard deviation
        sum_sq_diff = 0;
        for (i = 1; i <= count[domain]; i++) {
            diff = times[domain][i] - avg;
            sum_sq_diff += diff * diff;
        }
        stdev = sqrt(sum_sq_diff / count[domain]);

        # Round values to one decimal place
        rounded_avg = sprintf("%.1f", avg);
        rounded_min = sprintf("%.1f", min[domain]);
        rounded_max = sprintf("%.1f", max[domain]);
        rounded_stdev = sprintf("%.1f", stdev);

        # Print the results
        printf "%s,%.1f,%.1f,%.1f,%.1f\n", domain, rounded_avg, rounded_min, rounded_max, rounded_stdev;
    }
}
' "$input_file" | sort -t "," -k2,2nr > "$temp_file"

# Add 'https://' prefix and format for MediaWiki
sed -i 's/^/https:\/\//g' "$temp_file"

# Add header for MediaWiki and merge with content
{
    echo "<tab class=wikitable sep=comma head=top>"
    echo "Nettside/domene,Gjsn(sek),Min(sek),Maks(sek),StAvvik(sek)"
    cat "$temp_file"
    echo "</tab>"
} > "$output_file"

# Clean up temporary file
rm "$temp_file"

