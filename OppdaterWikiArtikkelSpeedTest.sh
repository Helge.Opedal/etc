#!/bin/bash
# Oppdaterer artikkel i itwiki'en med gjennomsnittlig svar respons på diverse nettsider
# Se også /home/helgeo/WebPageSpeedTest
cd /var/www/app/wiki/sites/itwiki/code/
php maintenance/edit.php --summary="Speed test resultat" "Nettside svar respons" - < /home/helgeo/WebPageSpeedTest/GjennomSnittResultat
